python manage.py runserver
django-admin startproject mysite

python manage.py makemigrations quiz

python manage.py sqlmigrate polls 0001
python manage.py startapp polls

python manage.py migrate


python manage.py shell

python manage.py createsuperuser

python manage.py runserver
